/* REPLACE.C */

#include "ACE\REPLACE.H"

/***************** GLOBAL VARIABLES */
/** precalculated filter table, needed if no FPU present */
void *g_pFilterTable;
/** pointer to REPLACE routines */
void *g_pReplace;
/** pointer to ACE tracker module */
void *g_pAceModule;
/** global flag, specifying if FPU is used for precalculating filter data or not(table is loaded from file then).*/
bool g_bUseFPU=FALSE;



/****************  all include functions */

int Replace_Init(bool bUseFpuFlag)
 {
	int iError=0;
	g_bUseFPU=bUseFpuFlag;
	
	if(!g_bUseFPU)
	 {
		/* Load filter tables */
		/* Load replay rout */
		g_pFilterTable=loadFile(REPLACE_FILTERTAB,PREFER_TT);
	
		if(g_pFilterTable!=NULL)
	 		{
	   			#ifdef _DEBUG
	   			 printf("Loaded FilterTable!\n");
		 	    #endif
	   
	   			g_pReplace=loadFile(REPLACE_BIN,PREFER_TT);

			    if(g_pReplace!=NULL)
		   	     {
	      			/*init ok*/
	   	    		iError=Replace_Prepare();
			   	    if(iError)
	   				    {
	   	    
	   	  					#ifdef _DEBUG
			 	   	   	     printf("Init went OK!\n");
				   	 		#endif	  
	     					return (0);
	    				}
	    			else
	    				{
	    					#ifdef _DEBUG
			   	   	    	printf("Couldn't initialise tables!\n");
	   			 			#endif	  
	    					Mfree(g_pFilterTable);
				    		Mfree(g_pReplace);
	    					return(-1);
	    				}
	    		}
	 			Mfree(g_pFilterTable);
	 			return(-1);
	 		}
	return (-1);
 }
 else /* use FPU for table precalc */
 	{
		g_pFilterTable=NULL; 	
		g_pReplace=loadFile(REPLACE_BIN,PREFER_TT);

			    if(g_pReplace!=NULL)
		   	     {
	      			/*init ok*/
	   	    		iError=Replace_Prepare();
			   	    if(iError)
	   				    {
	   	    
	   	  					#ifdef _DEBUG
			 	   	   	     printf("Init went OK!\n");
				   	 		#endif	  
	     					return (0);
	    				}
	    			else
	    				{
	    					#ifdef _DEBUG
			   	   	    	printf("Couldn't initialise tables!\n");
	   			 			#endif	  
	    					Mfree(g_pReplace);
	    					return(-1);
	    				}
	    		}
	 			return(-1); 	
 	
 	}
 
 }

int Replace_DeInit()
 {
 	/* stop replay */
 	Replace_StopSong();	

 	/* clean sound buffers */
 	Replace_RemoveModule();

	/* deinit replay */
	Replace_Exit(); 	

 	/* free memory */
	if(g_bUseFPU) { Mfree(g_pFilterTable); }
	Mfree(g_pReplace);
	return 0;
 }


void *Replace_LoadModule(char *pPath)
 {
	void *pPtr=loadFile(pPath,PREFER_TT);
	
	return(pPtr);	
	
 }
 
 int Replace_Delay (bool bEnable)
 {
 int iError=0;
 
 if(bEnable)
 	{
 		iError=Replace_DelayOn();
 		return(iError);
 	}
  else
 	{
 	 iError=Replace_DelayOff();
 	 return(iError);
 	}
 }

