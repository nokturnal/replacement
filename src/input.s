XDEF getKey
XDEF printLine

getKey:
	
	clr.l	d0
	move.b	$fffffc02,d0
	
	RTS

printLine:
	move.l	a0,-(sp)
	move.w	#$09,-(sp)
	trap	#1
	addq.l	#6,sp
	RTS

