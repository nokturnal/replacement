

#include "ACE\DISCROUT.H"

/* iMemFlag specified in eAtariMemory */
void *loadFile(char *pPath,int iMemFlag)
{
	unsigned long ulFileLenght=0L;
	void *pDummy=NULL;
	int fHandle=0;
	unsigned long ulBytesRead=0;
	
	ulFileLenght=getFileLenght(pPath);
	
	#ifdef _DEBUG
	 printf("try to read: %s, %ld \n",pPath,ulFileLenght); 
	#endif
	
	if(ulFileLenght!=0L)
	 {
	 	if(((iMemFlag<4)&&(!(iMemFlag<0))) )
	 		{
	 		 /* load to FAST_RAM */
	 		  pDummy=Mxalloc( ulFileLenght, iMemFlag);
	 		  if(pDummy==0L)
	 		 	{
	 		 		#ifdef _DEBUG
	 				 printf("Couldn't allocate memory!\n"); 
					#endif
	 		 		return(NULL);
	 		 	}
	 		  else
	 		   {
	 		   		#ifdef _DEBUG
	 				 printf("Allocated %ld of memory!\n",ulFileLenght); 
					#endif
	 		   		
	 		   		/* load file */
	 		   		fHandle=(int)Fopen(pPath, FO_READ);
	 		   		if(!(fHandle<0))
	 		   			{
	 		   			 	#ifdef _DEBUG
	 						 printf("Opened filehandle: %d for %s file.\n",fHandle,pPath); 
							#endif
	 		   			 	ulBytesRead=Fread(fHandle,ulFileLenght,pDummy);
	 		   			 	#ifdef _DEBUG
	 						 printf("Filelenght: %ld, bytesread:%ld.\n",ulFileLenght,ulBytesRead); 
							#endif
	 		   			 if(ulBytesRead!=ulFileLenght)
	 		   			 	printf("loadFile error!\n");
	 		   			 Fclose(fHandle);
							#ifdef _DEBUG
	 						 printf("Closed filehandle: %i .\n",fHandle); 
							#endif
						 /*return buffer*/
						return(pDummy);		
						}
	 		   
	 		   }
	 		}
	 }

 return(NULL);
}

unsigned long getFileLenght(char *pPath)
{
 DTA *pdta;
  int ret;
unsigned long lLenght=0;
  pdta = Fgetdta();
  ret = Fsfirst(pPath,0);
 
  while(ret == 0) 
  	{
  	 lLenght=(pdta->d_length);
     #ifdef _DEBUG
      printf("%12s %ld\n", pdta->d_fname, pdta->d_length);
	 #endif
     ret=Fsnext();
     return(lLenght);
    }
 return(0L);
}

