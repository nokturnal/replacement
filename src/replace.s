;*****************************************************
; REPLACE.S
; ASM ROUTS
; PureC REPLACEment library version: 1.00
; based on original REPLACE library by Thomas/NewBeat
; Pawel Goralski @ Saulot/[NOKTURNAL]
; any comments and criticism send to:
; e-mail: nokturnal@nokturnal.pl
; WWW: http:\\nokturnal.pl
;*****************************************************
;ver:  1.00b
;date: 16/17.10.2005

;************ EXPORT LABELS
XDEF Replace_Exit
XDEF Replace_Prepare 
XDEF Replace_StartInterrupt
XDEF Replace_InitialiseModule
XDEF Replace_PlaySong
XDEF Replace_StopSong
XDEF Replace_LoadModule
XDEF Replace_GetModuleTitle
XDEF Replace_GetModuleComposer
XDEF Replace_RemoveModule
XDEF Replace_ClearEcho
XDEF Replace_GetVersion
XDEF Replace_SetVolume
XDEF Replace_DelayOn
XDEF Replace_DelayOff
XDEF Replace_GetCpuLoad
XDEF Replace_GetNbOfVoices
XDEF Replace_StopInterrupt
 

;************ EXTERNAL VARIABLES
XREF g_pFilterTable
XREF g_pReplace
XREF g_bUseFPU
;*********** REPLACE EQUATES
REPLACE_OK EQU 1
REPLACE_ERROR EQU 0

;*********** REPLACE COMMANDS
REPLACE_INITIALIZE EQU 0
REPLACE_EXIT EQU 1
REPLACE_START_INTERRUPT EQU	2
REPLACE_INITIALIZE_MODULE EQU 3
REPLACE_PLAY_SONG EQU 4
REPLACE_STOP_SONG EQU 5
REPLACE_GET_MODULE_TITLE EQU 6
REPLACE_GET_MODULE_COMPOSER EQU 7
REPLACE_CLEAR_ECHO EQU 8
REPLACE_REMOVE_MODULE EQU 9
REPLACE_GET_VERSION EQU 10
REPLACE_SET_VOLUME EQU 11
REPLACE_DELAY_DISABLE EQU 12
REPLACE_DELAY_ENABLE EQU 13
REPLACE_GET_CPU_LOAD EQU 14
REPLACE_GET_NUMBER_OF_VOICES EQU 15
REPLACE_STOP_INTERRUPT EQU 16

;TEXT
Replace_Exit:
	move.l	#REPLACE_EXIT,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_Prepare:
	move.w	g_bUseFPU,d0
	cmp.w	#1,d0
	beq		.useFPU
	move.l	g_pFilterTable,a0	; No FPU
	move.l	#REPLACE_INITIALIZE,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	bra.s	.exit
	
.useFPU:
	move.l	#0,a0	;  use FPU
	move.l	#REPLACE_INITIALIZE,d0	;init
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)	
.exit:	
	RTS

Replace_StartInterrupt:
	;in a0 ACE module
	move.l	#REPLACE_START_INTERRUPT,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_InitialiseModule:
	move.l	#REPLACE_INITIALIZE_MODULE,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_PlaySong:
	move.l	#REPLACE_PLAY_SONG,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_StopSong:
	move.l	#REPLACE_STOP_SONG,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

/* IN: A0-21 char array pointer */
Replace_GetModuleTitle:
	move.l  a0,-(sp)		;put our array pointer on stack
	move.l	#REPLACE_GET_MODULE_TITLE,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	cmp.w	#REPLACE_ERROR,d0
	beq		.error		;error do nothing
	
	move.l	a0,a1		;put composer array address in a1
	move.l	(sp)+,a0	;get our array back
	;copy 20 characters
	move.w	#20-1,d0
.copy:
	move.b	(a1)+,(a0)+
	dbra	d0,.copy
	
	;put NULL termination ;)
	move.b	#$00,(a0)+
	;finish! :D
	bra.s	.noerror
	
.error:
	move.l	(sp)+,a0	;get our array back
.noerror:	

	RTS

Replace_GetModuleComposer:
	move.l  a0,-(sp)		;put our array pointer on stack
	move.l	#REPLACE_GET_MODULE_COMPOSER,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	cmp.w	#REPLACE_ERROR,d0
	beq		.error		;error do nothing
	
	move.l	a0,a1		;put composer array address in a1
	move.l	(sp)+,a0	;get our array back
	;copy 20 characters
	move.w	#20-1,d0
.copy:
	move.b	(a1)+,(a0)+
	dbra	d0,.copy
	
	;put NULL termination ;)
	move.b	#$00,(a0)+
	;finish! :D
	bra.s	.noerror
				
.error:
	move.l	(sp)+,a0	;get our array back
.noerror:
	RTS

Replace_RemoveModule:
	move.l	#REPLACE_REMOVE_MODULE,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_ClearEcho:
	move.l	#REPLACE_CLEAR_ECHO,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_GetVersion:
	move.l	a0,-(sp)	; put our array number on stack 
	move.l	#REPLACE_GET_VERSION,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	cmp.w	#1,d0
	bne		.error	
	
	;a0 now holds the address of 4 char, not NULL terminated string
	move.l	(sp)+,a1

	move.w	#4-1,d0
.copy:
	move.b	(a0)+,(a1)+
	dbra d0,.copy
	/* NULL terminate array */
	move.b #$00,(a1)+
	bra.s	.noerror
.error:
	move.w	(sp)+,a1
.noerror:
	RTS

Replace_SetVolume:
	move.l	d0,d1 /* move vol to d1 */
	move.l	#REPLACE_SET_VOLUME,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_DelayOn:
	move.l	#REPLACE_DELAY_ENABLE,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_DelayOff:
	move.l	#REPLACE_DELAY_DISABLE,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_GetCpuLoad:
	move.l	#REPLACE_GET_CPU_LOAD,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_GetNbOfVoices:
	move.l	#REPLACE_GET_NUMBER_OF_VOICES,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

Replace_StopInterrupt:
	move.l	#REPLACE_STOP_INTERRUPT,d0
	move.l	g_pReplace,a1
	adda.l	#28,a1
	jsr 	(a1)
	RTS

;DATA