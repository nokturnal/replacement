#ifndef __DISCROUT_H__
#define __DISCROUT_H__

#include <tos.h>
#include <stdio.h>

/* basic file i/o routines */

/** enumeration for Mxalloc() */
typedef enum
{
 ST_RAM=0,     /** alloc ST RAM only */
 FAST_RAM=1,    /**alloc TT RAM(FAST RAM only) */
 PREFER_ST=2,    /** alloc ST RAM if both present */
 PREFER_TT=3     /** alloc TT RAM if both present */
}eAtariMemory;
/** Allocates free memory with given flag, loads file from specified GEMDOS path and returns pointer to newly
    allocated memory where file was loaded.
        @remarks
           returns NULL if error occured..
        @param pPath GEMDOS, NULL terminated path to the file.
        @param iMemFlag memory allocation flag, as specified in eAtariMemory
 */
void *loadFile(char *pPath,int iMemFlag);
/** Returns the size in bytes of file in given GEMDOS path.
        @remarks
           returns NULL if error occured..
        @param pPath GEMDOS, NULL terminated path to the file.
  */
unsigned long getFileLenght(char *pPath);

#endif
/* EOF */
