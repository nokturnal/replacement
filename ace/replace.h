#ifndef __REPLACE_H__
#define __REPLACE_H__

/** REPLACEment / ACE player header file */
/** PureC version: Saulot/[NOKTURNAL] */
/** based on original REPLACE by Thomas/NewBeat */
/** ver. 1.00 16/10/2005 */

#include "ACE\DISCROUT.H"

/** Return values (error handling values)*/
#define REPLACE_OK	1
#define REPLACE_ERROR 0

/** module files relative paths */
#define REPLACE_FILTERTAB "F32.DAT"
#define REPLACE_BIN "REPLACE.BIN"

/* C functions interface */

/** boolean enum */

typedef enum
 {
 	TRUE=1,
 	FALSE=0
 } bool;


 /** Inits the Ace replayer.
        @remarks
           Inits the Ace tracker replayer.
        @param bUseFpuFlag Specifies if there Filter Table has to be loaded from file(F32.DAT) or precalculated by FPU (FPU needed).
 */
int Replace_Init(bool bUseFpuFlag);
/** DeInits the Ace replayer.
        @remarks
           Deinits the Ace tracker replayer, releases memory alocated for the Filter Table(if it was loaded from file).
    */
int Replace_DeInit(void);

/** Inits the Ace replayer.
        @remarks
           It's original REPLACE replayer REPLACE_INITIALIZE command.
           Returns REPLACE_OK if everything was ok or REPLACE_ERROR if error occured.
    */
int Replace_Prepare(void);
/** Deinits the Ace replayer.
        @remarks
           It's original REPLACE replayer REPLACE_EXIT command.
           Returns REPLACE_OK.
    */
int Replace_Exit(void);

/** Initialises module for replay.
    @param pAceMod pointer to ace tracker module data.
    @remarks Returns REPLACE_OK if everything was ok or REPLACE_ERROR if error occured.
 */
int Replace_InitialiseModule(void *pAceMod);

/** Plays module.
 */
int Replace_PlaySong(void);
/** Stops module replay.
 */
int Replace_StopSong(void);

/** Allocates memory, loads module from given GEMDOS path and returns pointer to the loaded module.
    @param pPath pointer to the NULL terminated string containing GEMDOS path.
    @remarks Returns NULL if file not found or other error occured.
 */
void *Replace_LoadModule(char *pPath);
/** Gets module title
@param pArr must be pointer to 21 character long array.
@remarks
       returns REPLACE_OK and array filled with NULL terminated string.
*/
int Replace_GetModuleTitle(char *pArr);

/** Gets module composer
@param pArr must be pointer to 21 character long array.
@remarks
       returns REPLACE_OK and array filled with NULL terminated string.
*/
int Replace_GetModuleComposer(char *pArr);
/** Deallocates memory buffers used by module.

@remarks
       returns REPLACE_OK.
*/
int Replace_RemoveModule(void);

/** Clears delay buffers.

@remarks
       Returns Returns REPLACE_OK if everything was ok or REPLACE_ERROR if error occured. */
int Replace_ClearEcho(void);
/** Gets REPLACE version
@param pArr must be pointer to 5 character long array.
@remarks
       returns REPLACE_OK and array filled with NULL terminated string carrying the version of REPLACE replayer.
*/
void Replace_GetVersion(char *pArr);

/** Sets the replay volume.
@param lVol replay volume, range: 0-127
@remarks
       returns REPLACE_OK
*/
int Replace_SetVolume(long lVol);

/** Enables/Disables delay
    @param bEnable Delay disable/enable flag. If bEnable is TRUE then delay is ON, else delay is OFF.
    @remarks
           returns REPLACE_OK. It is not reccomended to call this function while playing module.
           If delay is enabled the player uses more CPU power. (delay is ping-pong effect). 
           NOTE: This option is buggy atm. Setting delay to off gets no sound, sorry not my fault ;)....
*/
int Replace_Delay(bool bEnable);

/** Enables delay
    @remarks
            called by Replace_Delay(). returns REPLACE_OK.

*/
int Replace_DelayOn(void);
/** Disables delay
    @remarks
            called by Replace_Delay(). returns REPLACE_OK.

*/
int Replace_DelayOff(void);

/** Returns current CPU load. Value ranging from from 0 to 511 in lower word.
@remarks( It is calculated from last frame) [NOT TESTED!].
 */
int Replace_GetCpuLoad(void);

/** Returns the number of voices that are currently playing [BUGGY atm!]. */
long Replace_GetNbOfVoices(void);

/** Starts player interrupt
    @param pAceMod Pointer to ace module.
    @remarks
           @remarks Returns REPLACE_OK if everything was ok or REPLACE_ERROR if error occured.
*/
int Replace_StartInterrupt(void *pAceMod);

/** Stops player interrupt
        @remarks
           @remarks Returns REPLACE_OK if everything was ok or REPLACE_ERROR if error occured.
*/
int Replace_StopInterrupt(void);

#endif
/* EOF */
