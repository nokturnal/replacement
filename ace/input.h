#ifndef __INPUT_H__
#define __INPUT_H__
/* small input/output routines */

unsigned char getKey(void);
void printLine(char *pString);

#endif
